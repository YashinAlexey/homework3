﻿using System;
using System.Text;

namespace HomeWork3
{
    class Program
    {
        static void Main(string[] args)
        {
            HandlerURL handlerURL = new HandlerURL();

            Console.WriteLine("Введите URL-адрес источника:");
            //string addressURL = "https://www.chess.com/stats/puzzles/fformula?type=puzzle-rush#recent";
            string addressURL = Console.ReadLine();

            if (handlerURL.IsCorrectAddress(addressURL))
            {
                string addresses = handlerURL.GetUrlAddresses(handlerURL.GetContentByUrlAddress(addressURL));

                if (string.IsNullOrEmpty(addresses))
                    Console.WriteLine("Указанный источник не содержит URL-адресов");                    
                else
                    Console.WriteLine("Найденные URL-адреса:" + addresses);
            }
            else
                Console.WriteLine("Введён некорректный URL-адрес");

            Console.ReadLine();
        }
    }
}
