﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace HomeWork3
{
    class HandlerURL
    {
        public bool IsCorrectAddress(string address)
        {
            return !string.IsNullOrEmpty(GetUrlAddresses(address));
        }

        public string GetUrlAddresses(string inputString)
        {
            Regex regex = new Regex(@"(((https?:)(\/\/)?(www\.)?)|(www\.))([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            MatchCollection matches = regex.Matches(inputString);

            StringBuilder stringBuilder = new StringBuilder();

            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    stringBuilder.Append(match + Environment.NewLine);
            }

            if (stringBuilder.Length == 0)
                return string.Empty;

            string tempString = stringBuilder.ToString().ToLower();
            stringBuilder.Clear();

            string[] temp = tempString.Split(Environment.NewLine);
            var addresses = temp.Union(temp).OrderBy(x => x);
            
            foreach(string address in addresses)
                stringBuilder.Append(address + Environment.NewLine);

            return stringBuilder.ToString();
        }

        public string GetContentByUrlAddress(string address)
        {
            string result = string.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;

                if (String.IsNullOrWhiteSpace(response.CharacterSet))
                    readStream = new StreamReader(receiveStream);
                else
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));

                result = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
            }

            return result;
        }
    }
}
